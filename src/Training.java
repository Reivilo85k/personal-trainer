import java.util.Scanner;

public class Training extends Activities {
    public static Activities cardio(Activities user){
        user.setStamina(user.getStamina()-2);
        user.setStrength(user.getStrength()+1);
        user.setEndurance(user.getEndurance()+3);
        System.out.println("Stamina -2\nStrength +1\nEndurance +3");
        return user;
    }
    public static Activities lifting(Activities user){
        user.setStamina(user.getStamina()-3);
        user.setStrength(user.getStrength()+3);
        user.setEndurance(user.getEndurance()+1);
        System.out.println("Stamina -3\nStrength +3\nEndurance +1");
        return user;
    }
    public static Activities swimming(Activities user){
        user.setStamina(user.getStamina()-1);
        user.setStrength(user.getStrength()+2);
        user.setEndurance(user.getEndurance()+2);
        System.out.println("Stamina -1\nStrength +2\nEndurance +2");
        return user;
    }
    public static Activities crossFit(Activities user){
        user.setStamina(user.getStamina()-4);
        user.setStrength(user.getStrength()+2);
        user.setEndurance(user.getEndurance()+2);
        System.out.println("Stamina -4\nStrength +2\nEndurance +2");
        return user;
    }
    public static Activities pilates(Activities user){
        user.setStamina(user.getStamina()-1);
        user.setStrength(user.getStrength()+1);
        user.setEndurance(user.getEndurance()+1);
        System.out.println("Stamina -1\nStrength +1\nEndurance +1");
        return user;
    }
    public static Activities yoga(Activities user){
        user.setStamina(user.getStamina()-1);
        user.setStrength(user.getStrength()+1);
        user.setEndurance(user.getEndurance()+1);
        System.out.println("Stamina -1\nStrength +2\nEndurance +1");
        return user;
    }


}