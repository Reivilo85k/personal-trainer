//Create class Trainee, it should contain fields like: name, stamina, strength.
//You’ll simulate both sides – Trainer and Trainee. Within a while loop you will be
//asked for an exercise to be done. Every exercise should add/reduce
//stamina/strength.
//Take into account that stamina should not be reduced below 0.
//Consider adding some supplements that will recover the stamina. Supplement
//should be additional class.import java.util.Random;


import java.util.Random;
import java.util.Scanner;

public class Main {
private static final int USER_PARAMETERS = 25;
private static final Random rand =  new Random();

    //generates an integer between 1 and 25;
    public static int randomStat(){
        return rand.nextInt(USER_PARAMETERS)+1;
    }

    public static void main(String[] args){
        Scanner user = new Scanner(System.in);
        System.out.print("Welcome to The Featness Club\nPlease enter your name : \n");

        //Create a new trainee
        Trainee trainee1 = new Trainee(user.nextLine(), randomStat(), randomStat(), randomStat());

        //Create a new trainer
        Trainer trainer1 = new Trainer("The Rock", 30,30, 30 );

        System.out.printf("\nHello %s\n", trainee1.getName());
        Session.session(trainee1, trainer1);


    }

}
