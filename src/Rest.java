import java.util.Scanner;

public class Rest extends Activities{
    public static Activities hotShower(Activities user){
        user.setStamina(user.getStamina()+5);
        user.setStrength(user.getStrength()-2);
        user.setEndurance(user.getEndurance()-2);
        System.out.println("Stamina +5\nStrength -2\nEndurance -2");
        return user;
    }
    public static Activities sauna(Activities user){
        user.setStamina(user.getStamina()+3);
        user.setEndurance(user.getEndurance()-1);
        System.out.println("Stamina +3\nEndurance -1");
        return user;
    }
    public static Activities powerNap(Activities user){
        user.setStamina(user.getStamina()+3);
        user.setStrength(user.getStrength()-1);
        System.out.println("Stamina +3\nStrength -1");
        return user;
    }
    public static Activities drinkWater(Activities user){
        user.setStamina(user.getStamina()+1);
        System.out.println("Stamina +1\n");
        return user;
    }
}
