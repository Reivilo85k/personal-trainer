import java.util.Scanner;

public class Session {
    public static boolean session(Trainee trainee, Trainer trainer){

        Scanner user = new Scanner(System.in);
        System.out.println("Your trainer is The Rock");
        //create a loop to chose between between training, rest or finishing the loop
        while (true) {
            //check if stamina is not too low
            if (trainee.getStamina() <= 0 || trainer.getStamina() <= 0) {
                System.out.println("Cannot proceed due to low stamina\nEnough for today ! \nIt's also important to rest to improve ! \nSee you next time");
                return false;
            } else {
                System.out.printf("\nYour stamina is %s, your strength is %d and your endurance is %d.\n", trainee.getStamina(), trainee.getStrength(), trainee.getEndurance());
                System.out.printf("\n%s : stamina : %d , strength : %d, endurance : %d.\n", trainer.getName(), trainer.getStamina(), trainer.getStrength(), trainer.getEndurance());
                System.out.println("\nFor your next activity would you like to :\n1- Rest\n2- Train\n3- Finish your session.\n");
                int choice = user.nextInt();
                switch (choice) {
                    //switch case to chose rest activity
                    case 1:
                        System.out.println("Choose how you want to rest : \n1- Hot Shower\n2- Sauna\n3- Power Nap\n4- Drink  water");
                        int rest = user.nextInt();
                        switch (rest) {
                            case 1:
                                trainee = (Trainee) Rest.hotShower(trainee);
                                trainer = Trainer.hotShower(trainer);
                                break;
                            case 2:
                                trainee = (Trainee) Rest.sauna(trainee);
                                trainer = Trainer.sauna(trainer);
                                break;
                            case 3:
                                trainee = (Trainee) Rest.powerNap(trainee);
                                trainer = Trainer.powerNap(trainer);
                                break;
                            case 4:
                                trainee = (Trainee) Rest.drinkWater(trainee);
                                trainer = Trainer.drinkWater(trainer);
                                break;
                            default:
                                System.out.println("Input error, please try again");
                        }
                        break;
                    //switch case to chose training activity
                    case 2:
                        System.out.println("Choose your next Exercise :\n1- Cardio\n2- Lifting\n3- Swimming\n4- Crossfit\n5- Pilates\n6- Yoga");
                        int activity = user.nextInt();
                        switch (activity) {
                            case 1:
                                trainee = (Trainee) Training.cardio(trainee);
                                trainer = Trainer.cardio(trainer);
                                break;
                            case 2:
                                trainee = (Trainee) Training.lifting(trainee);
                                trainer = Trainer.lifting(trainer);
                                break;
                            case 3:
                                trainee = (Trainee) Training.swimming(trainee);
                                trainer = Trainer.swimming(trainer);
                                break;
                            case 4:
                                trainee = (Trainee) Training.crossFit(trainee);
                                trainer = Trainer.crossFit(trainer);
                                break;
                            case 5:
                                trainee = (Trainee) Training.pilates(trainee);
                                trainer = Trainer.pilates(trainer);
                                break;
                            case 6:
                                trainee = (Trainee) Training.yoga(trainee);
                                trainer = Trainer.yoga(trainer);
                                break;
                            default:
                                System.out.println("Input error, please try again");
                        }
                        break;
                    //finishing the loop
                    case 3:
                        System.out.println("Good job !!!\n\nThanks for visiting The Featness Club.\nSee you very soon for the next session.\n\n");
                        return false;
                    //ask for new input
                    default:
                        System.out.println("\nInput error, please try again");
                }
            }
        }

    }
}

