public class Trainee  extends Activities{
    private String name;
    private int stamina;
    private int strength;
    private int endurance;

    public Trainee(String name, int stamina, int strength, int endurance) {
        if (stamina == 0) {
            throw new IllegalArgumentException("You're stamina is too low, please rest for a while");
        }
        this.name = name;
        this.stamina = stamina;
        this.strength = strength;
        this.endurance = endurance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }
}
