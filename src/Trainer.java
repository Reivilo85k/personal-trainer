public class Trainer extends Activities{
    private String name;
    private int stamina;
    private int strength;
    private int endurance;

    public Trainer(String name, int stamina, int strength, int endurance) {
        if (stamina == 0) {
            throw new IllegalArgumentException("You're stamina is too low, please rest for a while");
        }
        this.name = name;
        this.stamina = stamina;
        this.strength = strength;
        this.endurance = endurance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getEndurance() {
        return endurance;
    }

    public void setEndurance(int endurance) {
        this.endurance = endurance;
    }


    public static Trainer cardio(Trainer user){
        user.setStamina(user.getStamina()-1);
        user.setEndurance(user.getEndurance()-1);
        return user;
    }
    public static Trainer lifting(Trainer user){
        user.setStamina(user.getStamina()-1);
        user.setStrength(user.getStrength()-1);
        return user;
    }
    public static Trainer swimming(Trainer user){
        user.setStrength(user.getStrength()-1);
        user.setEndurance(user.getEndurance()-1);
        return user;
    }
    public static Trainer crossFit(Trainer user){
        user.setStamina(user.getStamina()-1);
        user.setStrength(user.getStrength()-1);
        user.setEndurance(user.getEndurance()-1);
        return user;
    }
    public static Trainer pilates(Trainer user){
        user.setEndurance(user.getEndurance()-1);
        return user;
    }
    public static Trainer yoga(Trainer user){
        user.setStamina(user.getStamina()-1);
        return user;
    }
    public static Trainer hotShower(Trainer user){
        user.setStamina(user.getStamina()+1);
        user.setEndurance(user.getEndurance()+1);
        return user;
    }
    public static Trainer sauna(Trainer user){
        user.setStrength(user.getStrength()+1);
        user.setEndurance(user.getEndurance()+1);
        return user;
    }
    public static Trainer powerNap(Trainer user){
        user.setStamina(user.getStamina()+1);
        user.setStrength(user.getStrength()+1);
        user.setEndurance(user.getEndurance()+1);
        return user;
    }
    public static Trainer drinkWater(Trainer user){
        user.setStamina(user.getStamina()+1);
        user.setStrength(user.getStrength()+1);
        return user;
    }
}
