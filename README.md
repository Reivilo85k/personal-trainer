Personal Trainer Exercise.


Create class Trainee, it should contain fields like: name, stamina, strength.
You’ll simulate both sides – Trainer and Trainee. Within a while loop you will be
asked for an exercise to be done. Every exercise should add/reduce
stamina/strength.
Take into account that stamina should not be reduced below 0.
Consider adding some supplements that will recover the stamina. Supplement
should be additional class.
